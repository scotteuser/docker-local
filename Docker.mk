# Include the settings.
include Settings.mk

docker-list-sites:
	# List of docker sites in the Settings.mk file.
	@sed -n -e 's/^.*\(.docker-name = \)/\1/p' Settings.mk | sed 's/.docker-name = //g'

docker-stop:
	# Stop the docker containers.
	# Execute like "make docker-stop s='sitename'"
	docker stop \
		$($(s).docker-name)-mysql \
		$($(s).docker-name)-phpmyadmin \
		$($(s).docker-name)-drupal

docker-start:
	# Start the docker containers.
	# Execute like "make docker-start s='sitename'"
	docker start $($(s).docker-name)-mysql
	docker start $($(s).docker-name)-phpmyadmin
	docker start $($(s).docker-name)-drupal

	# Add the hostname to /etc/hosts to allow tests to run.
	docker exec $($(s).docker-name)-drupal sh -c \
		"echo \"127.0.0.1	$($(s).docker-name).local\" >> /etc/hosts"
	docker exec $($(s).docker-name)-drupal sh -c \
		"service apache2 reload"

docker-build:
	# Init build.
	# Execute like "make docker-build s='sitename'"

	docker build --no-cache -t $($(s).docker-name)/drupal .

	# Boot up the mysql container.
	docker run --name $($(s).docker-name)-mysql \
		-e MYSQL_ROOT_PASSWORD=$($(s).db-pass) \
		-e MYSQL_USER=$($(s).db-user) \
		-e MYSQL_PASSWORD=$($(s).db-pass) \
		-v $($(s).assets-files)/$($(s).assets-sqldump-relative-location):/var/$($(s).assets-sqldump-relative-location) \
		-d mysql:latest

	# Wait for mysql to be started then create the database.
	sleep 12 && \
		docker exec $($(s).docker-name)-mysql sh -c \
		"exec mysql -uroot -p$($(s).db-pass) -e 'CREATE DATABASE $($(s).db-name) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;'"

	# Grant priveleges on the database.
	docker exec $($(s).docker-name)-mysql sh -c \
		"exec mysql -uroot -p$($(s).db-pass) -e \"GRANT ALL PRIVILEGES ON $($(s).db-name) . * TO '$($(s).db-user)'@'%'\""

	# Prepare the phpmyadmin conf for auto login
	mkdir -p private/phpmyadmin
	cp private/templates/phpmyadmin.config.user.inc.php private/phpmyadmin/$($(s).docker-name).phpmyadmin.config.user.inc.php
	sed -i 's/DB-USER/$($(s).db-user)/g' private/phpmyadmin/$($(s).docker-name).phpmyadmin.config.user.inc.php
	sed -i 's/DB-PASS/$($(s).db-pass)/g' private/phpmyadmin/$($(s).docker-name).phpmyadmin.config.user.inc.php
	sed -i 's/DOCKERNAME/$($(s).docker-name)/g' private/phpmyadmin/$($(s).docker-name).phpmyadmin.config.user.inc.php

	# Boot up the phpmyadmin container.
	docker run --name $($(s).docker-name)-phpmyadmin \
		-v $(shell pwd)/private/phpmyadmin/$($(s).docker-name).phpmyadmin.config.user.inc.php:/config.user.inc.php \
		-d --link $($(s).docker-name)-mysql:mysql \
		-p 8081:80 phpmyadmin/phpmyadmin

	# Prepare vhost for within container.
	cp private/templates/within-container.conf private/apache2/$($(s).docker-name)-within-container.conf
	sed -i 's/WEBROOT/$($(s).assets-webroot)/g' private/apache2/$($(s).docker-name)-within-container.conf
	sed -i 's/DOCKERNAME/$($(s).docker-name)/g' private/apache2/$($(s).docker-name)-within-container.conf

	# Boot up the drupal html container.
	docker run \
		--name $($(s).docker-name)-drupal \
		--link $($(s).docker-name)-mysql:mysql \
		-v $($(s).assets-files):/var/www/html \
		-v $(shell pwd)/private/apache2/$($(s).docker-name)-within-container.conf:/etc/apache2/sites-available/within-container.conf \
		-p 8080:80 \
		-d $($(s).docker-name)/drupal

	# Import the mysql dump.
	docker exec $($(s).docker-name)-mysql sh -c \
		"exec mysql -uroot -p$($(s).db-pass) $($(s).db-name) < /var/$($(s).assets-sqldump-relative-location)/$($(s).assets-sqldump-filename)"

	# Reload apache.
	docker exec $($(s).docker-name)-drupal sh -c \
		"service apache2 reload"

	# Make nano available in the terminal.
	docker exec $($(s).docker-name)-drupal sh -c \
		"export TERM=xterm"

	# Disable the 000-default.conf.
	docker exec $($(s).docker-name)-drupal sh -c \
		"a2dissite 000-default.conf"

	# Enable the within-container.conf.
	docker exec $($(s).docker-name)-drupal sh -c \
		"a2ensite within-container.conf"

	# Reload apache2.
	docker exec $($(s).docker-name)-drupal sh -c \
		"service apache2 reload"

	##################################
	# Docker container ready for use #
	##################################

docker-webroot-exec:
	# Execute like "make docker-webroot-exec s='sitename' arg='drush cr'"
	docker exec $($(s).docker-name)-drupal sh -c \
		"cd /var/www/html/web && $(arg)"

docker-ssh-drupal:
	# Execute like "make docker-ssh-drupal s='sitename'"
	docker exec -it $($(s).docker-name)-drupal /bin/bash

docker-ssh-mysql:
	# Execute like "make docker-ssh-mysql s='sitename'"
	docker exec -it $($(s).docker-name)-mysql /bin/bash && \
		mysql -u$($(s).db-user) -p$($(s).db-pass)

docker-mysqldump-overwrite:
	# Execute like "make docker-mysqldump-overwrite s='sitename'"
	docker exec -i $($(s).docker-name)-mysql \
		mysqldump -u$($(s).db-user) -p$($(s).db-pass) $($(s).db-name) > $($(s).assets-files)/$($(s).assets-sqldump-relative-location)/$($(s).assets-sqldump-filename)

docker-mysql-restore-from-original:
	# Execute like "make docker-mysql-restore-from-original s='sitename'"
	docker exec -i $($(s).docker-name)-mysql \
		mysql -u$($(s).db-user) -p$($(s).db-pass) $($(s).db-name) < $($(s).assets-files)/$($(s).assets-sqldump-relative-location)/$($(s).assets-sqldump-filename)

docker-remove:
	# Remove existing containers to retest build.
	# Execute like "make docker-remove s='sitename'"
	docker kill $($(s).docker-name)-mysql || true
	docker rm $($(s).docker-name)-mysql || true
	docker kill $($(s).docker-name)-phpmyadmin || true
	docker rm $($(s).docker-name)-phpmyadmin || true
	docker kill $($(s).docker-name)-drupal || true
	docker rm $($(s).docker-name)-drupal || true

docker-ubuntu-host-setup:
	# Auto configure /etc/hosts and apache2 conf.
	# Execute like "make docker-ubuntu-host-setup s='sitename'"
	mkdir -p private/apache2
	cp private/templates/template.local.conf private/apache2/$($(s).docker-name)-local-docker.conf
	sed -i 's/DOCKERNAME/$($(s).docker-name)/g' private/apache2/$($(s).docker-name)-local-docker.conf
	sudo ln -s $(shell pwd)/private/apache2/$($(s).docker-name)-local-docker.conf /etc/apache2/sites-available/$($(s).docker-name)-local-docker.conf
	sudo a2ensite $($(s).docker-name)-local-docker.conf
	echo '127.0.0.1       $($(s).docker-name).local' | sudo tee --append /etc/hosts > /dev/null
	echo '127.0.0.1       phpmyadmin.$($(s).docker-name).local' | sudo tee --append /etc/hosts > /dev/null
	sudo service apache2 restart

docker-ubuntu-host-remove:
	# Auto remove configured /etc/hosts and apache2 conf.
	# Execute like "make docker-ubuntu-host-remove s='sitename'"
	sudo sed -i.bak '/127.0.0.1       phpmyadmin.$($(s).docker-name).local/d' /etc/hosts
	sudo sed -i.bak '/127.0.0.1       $($(s).docker-name).local/d' /etc/hosts
	sudo a2dissite $($(s).docker-name)-local-docker.conf
	sudo rm /etc/apache2/sites-available/$($(s).docker-name)-local-docker.conf
	sudo service apache2 restart
