# from https://www.drupal.org/requirements/php#drupalversions
FROM php:7.0-apache

# File Author / Maintainer
MAINTAINER Scott Euser

#############################
# NOT FOR USE IN PRODUCTION #
#############################

RUN a2enmod rewrite

# Install the PHP extensions we need.
RUN apt-get update && apt-get install -y libpng12-dev libjpeg-dev libpq-dev \
	&& rm -rf /var/lib/apt/lists/* \
	&& docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
	&& docker-php-ext-install gd mbstring pdo pdo_mysql pdo_pgsql zip

WORKDIR /var/www/html

# General tools
RUN apt-get update && apt-get install -y \
    nano

# Install Drush
RUN php -r "readfile('http://files.drush.org/drush.phar');" > drush && \
    chmod +x drush && \
    mv drush /usr/local/bin/drush

# Install Drupal Console.
RUN \
    curl http://drupalconsole.com/installer -L -o drupal.phar && \
    mv drupal.phar /usr/local/bin/drupal && \
    chmod +x /usr/local/bin/drupal

# Install WP-CLI
RUN \
    curl https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar -L -o wp-cli.phar && \
    mv wp-cli.phar /usr/local/bin/wp && \
    chmod +x /usr/local/bin/wp

# Install MySQL client to allow drush/drupal console access to the database
RUN \
    apt-get install mysql-client -y
