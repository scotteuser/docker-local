# Introduction

**  Not for use in production environments  **

This repo is a basic setup for quick deployment of multiple local environments
with the need to rapidly switch between environments and have each available
at separate local URLs. This repo is geared towards Ubuntu machines (tested on
Ubuntu 14 and 16). It automatically sets up apache2 vhosts and addsto your
/etc/hosts file to quickly fire up additional sites. The repo is a simple LAMP
stack with the goal to add a common arsenal of tools for rapid Drupal,
WordPress, and other general php-based development.

You can add as many sites as you would like within your Settings.mk. You only
need this repo once.

This is primary Makefile based to facilitate local development beyond the scope
of the Docker containers and is largely experimental.

# Primary audience

Myself :)

# One time setup of docker

*  [Install docker](https://docs.docker.com/engine/installation/)
*  Get the mysql container `docker pull mysql`
*  Get the phpmyadmin container `docker pull phpmyadmin`

# Creating sites

*  Copy the Example.Settings.mk to Settings.mk and fill in the values.
*  Run `make docker-ubuntu-host-setup s="sitename"` to set up a vhost, add to
etc/hosts automatically
*  Run `make docker-build s="sitename"` to build the containers with the
database and files
*  Access via `http://docker-name.local` and
`http://phpmyadmin.docker-name.local`, where docker-name is the specified
docker-name in `Setting.mk`
*  MySQL hostname is `sitename-mysql` so if your sitename is `drupal8` then the
MySQL host would be `drupal8-mysql`

# Other commands

*  `make docker-start s="sitename"`: Starts the already built containers again
when stopped
*  `make docker-stop s="sitename"`: Stops the containers
*  `make docker-remove s="sitename"`: Deletes the containers to let you rebuild
from scratch
*  `make docker-ubuntu-host-setup s="sitename"`: Create a vhost and add to
etc/hosts automatically
*  `make docker-ubuntu-host-remove s="sitename"`: Remove the vhost that was set
up and remove from etc/hosts automatically

# TODO

*  Make container database data persist without needing to do a dump
*  Add more tools like make, etc within or linked to the container
*  Test building existing sites vs new sites and optimise workflow
