<?php

/**
 * @file
 * Phpmyadmin config.
 */

$cfg['Servers'][$i]['auth_type'] = 'config';
$cfg['Servers'][$i]['user'] = 'DB-USER';
$cfg['Servers'][$i]['password'] = 'DB-PASS';
$cfg['Servers'][$i]['host'] = 'DOCKERNAME-mysql';
