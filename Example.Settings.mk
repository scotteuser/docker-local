# Configure the settings for docker before running docker-build.
# - note that the db-hostname will be $(docker-name)-mysql
sitename.db-name = db-name-here
sitename.db-user = db-user-here
sitename.db-pass = db-pass-here

# The desired container name. If using the ubuntu helper command for local
# development (make docker-ubuntu-host-setup), your site would be available
# at http://docker-container-name-here.local
sitename.docker-name = docker-container-name-here

# Where your project files are stored, this is usually 1 directory up from
# your document root.
sitename.assets-files = /path/to/project

# The relative path within the project files. For instance, if your
# project files path is /path/to/project and the document root should
# be /path/to/project/web then enter 'web'
sitename.assets-webroot = web

# The path to the database dump, this should be within your project
# files but outside of the document root.
sitename.assets-sqldump-relative-location = relative/path/from/assets-files/to/dump

# The name of the database dump file to import.
sitename.assets-sqldump-filename = latest.sql

# Add additional sites like the following:
# sitename2.db-name
# etc
